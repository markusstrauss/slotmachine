var $winSound = $("#winSound");
var $loseSound = $("#loseSound");
var $clickSound = $("#clickSound");
var clickSoundPlaying = false;

var BackendConnector = (function() {

	function send() {
		$.ajax({
			url: "http://fettblog.eu/fh/numbers.php?jsonp",
			dataType: "jsonp",
			jsonpCallback: "callback",

			success: function( data ) {
				Mediator.publish("dataRecieved", data);
			},
			error: function( err ) {
				console.log(err);
			}
		});
	}

	Mediator.subscribe("inputGiven", send);

})();

function getSlotClass(slotImg) {
	var result = "";

	if(slotImg == 0)
		result = "cherry";
	if(slotImg == 1)
		result = "thing";
	if(slotImg == 2)
		result = "star";
	if(slotImg == 3)
		result = "veget";

	console.log(result);
	return result;
}

var $coinLabel = $(".coins");
var result;

$clickSound.on("ended", function() {
	if(clickSoundPlaying === true)
		$(this)[0].play();
});

$("html").on("keydown", function(e) {
	if(e.which == 13) {

		var coins = parseInt($coinLabel.html());
		if(coins < 1) {
			alert("Not enough coins");
			return;
		}

		coins--;
		Mediator.publish("inputGiven");
		clickSoundPlaying = true;
		$clickSound[0].play();
		$coinLabel.html(coins);
	}
});

var transEndEventNames = {
	"WebkitAnimation": "webkitAnimationEnd",
	"MozAnimation": "animationend",
	"animation": "animationend"
};

$("#slot3").on(transEndEventNames[Modernizr.prefixed("animation")], function() {
	var coins = parseInt($coinLabel.html());
	coins += result;
	$coinLabel.html(coins);

	clickSoundPlaying = false;
	$clickSound[0].pause();

	if(result > 0) {
		$winSound[0].play();
	} else {
		$loseSound[0].play();
	}
});

window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();

Mediator.subscribe("dataRecieved", function(data) {
	var slotData = data[0].slots;
	result = data[0].result;

	requestAnimFrame(function() {
		$("#slot1, #slot2, #slot3").attr("class", "slot");
		requestAnimFrame(function() {
			$("#slot1").addClass(getSlotClass(slotData[0]));
			$("#slot2").addClass(getSlotClass(slotData[1]));
			$("#slot3").addClass(getSlotClass(slotData[2]));
		});
	});
});